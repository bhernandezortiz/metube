import { useState, createContext } from "react";

const SigninContext = createContext();
export const SigninProvider = ({ children }) => {
  const [signinOverlayIsVisible, setSigninOverlayIsVisible] = useState(false);
  const [registering, setRegistering] = useState(false);
  return (
    <SigninContext.Provider
      value={{
        signinOverlayIsVisible,
        setSigninOverlayIsVisible,
        registering,
        setRegistering,
      }}
    >
      {children}
    </SigninContext.Provider>
  );
};
export default SigninContext;
