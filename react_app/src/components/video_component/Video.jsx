import React from "react";
import ReactPlayer from "react-player";
const Video = ({ video }) => {
  return (
    <div className="main-video">
      <div className="video">
        <ReactPlayer
          url={video.video_link}
          controls={false}
          width="100%"
          height="100%"
        />
      </div>
    </div>
  );
};

export default Video;
