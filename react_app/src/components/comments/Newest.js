import React, { useContext } from "react";
import MetubeContext from "../../context/MetubeContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { faThumbsDown } from "@fortawesome/free-solid-svg-icons";
import Tippy from "@tippyjs/react";

const Newest = ({ getTimeAgo }) => {
  // const { commentsOrdered } = useContext(MetubeContext);
  const { currentComments } = useContext(MetubeContext);
  return currentComments.map((comment) => (
    <div className="comment" key={comment.id}>
      <img className="comment-avatar" src={comment.avatar} />
      <div className="comment-body">
        <div className="comment-header">
          <span className="comment-author">{comment.username}</span>
          <span className="comment-time">
            {getTimeAgo(comment.date_published)}
          </span>
        </div>
        <div className="comment-text">{comment.comment}</div>
        <div className="comment-actions">
          <Tippy content="Like" arrow={false} placement="bottom">
            <button className="comment-like">
              <FontAwesomeIcon icon={faThumbsUp} size="1x" />
            </button>
          </Tippy>
          <p className="likeCount">{comment.likes}</p>
          <Tippy content="Dislike" arrow={false} placement="bottom">
            <button className="comment-dislike">
              <FontAwesomeIcon icon={faThumbsDown} size="1x" />
            </button>
          </Tippy>
          <button className="comment-reply">Reply</button>
        </div>
      </div>
    </div>
  ));
};

export default Newest;
