const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();

function authenticateToken(req, res, next) {
  try {
    //Authorization BEARER afdlaksdflalfkgadfgdafg
    const authHeader = req.headers["authorization"]; //Bearer TOKEN
    const token = authHeader && authHeader.split(" ")[1];
    if (token == null) {
      return res.status(401).send({ msg: "Null token" });
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error, user) => {
      if (error) {
        return res.status(403).send({ error });
      }
      req.user = user;
      next();
    });
  } catch (error) {
    return res.status(403).send({ error });
  }
}

module.exports = authenticateToken;
